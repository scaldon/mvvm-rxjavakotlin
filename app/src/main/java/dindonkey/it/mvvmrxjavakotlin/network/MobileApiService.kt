package dindonkey.it.mvvmrxjavakotlin.network

import io.reactivex.Observable
import retrofit2.http.GET

interface MobileApiService {

    @GET("it/discounts?partnerid=androidtrovaprezziit&wt=json")
    fun discounts(): Observable<DiscountsResponse>
}