package dindonkey.it.mvvmrxjavakotlin.network

import dindonkey.it.mvvmrxjavakotlin.ui.homepage.showcase.products.Product

class DiscountsResponse(val discountedProducts: DiscountedProducts)

class DiscountedProducts(val discountedProduct: List<Product>)

