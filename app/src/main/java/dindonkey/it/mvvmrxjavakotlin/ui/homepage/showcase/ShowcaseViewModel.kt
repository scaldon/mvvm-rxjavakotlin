package dindonkey.it.mvvmrxjavakotlin.ui.homepage.showcase

import dindonkey.it.mvvmrxjavakotlin.extensions.RxActivityIndicator
import dindonkey.it.mvvmrxjavakotlin.extensions.elements
import dindonkey.it.mvvmrxjavakotlin.extensions.error
import dindonkey.it.mvvmrxjavakotlin.extensions.trackActivity
import dindonkey.it.mvvmrxjavakotlin.ui.homepage.showcase.products.Product
import dindonkey.it.mvvmrxjavakotlin.network.MobileApiService
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject


class ShowcaseViewModel @Inject constructor(mobileApiService: MobileApiService, retrySubject: PublishSubject<Unit>) {

    private val showcaseFeed = retrySubject
            .startWith(Unit)
            .flatMap {
                mobileApiService
                        .discounts()
                        .trackActivity(loading)
                        .materialize()
            }
            .share()
            .replay()
            .autoConnect()

    val loading = RxActivityIndicator()

    val discounts: Observable<List<Product>> = showcaseFeed
            .elements()
            .map { it.discountedProducts.discountedProduct }

    val error: Observable<Throwable> = showcaseFeed.error()

}

