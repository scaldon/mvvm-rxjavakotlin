package dindonkey.it.mvvmrxjavakotlin.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.clicks
import dindonkey.it.mvvmrxjavakotlin.R
import dindonkey.it.mvvmrxjavakotlin.extensions.addTo
import dindonkey.it.mvvmrxjavakotlin.extensions.inTransaction
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.error_fragment_layout.view.*
import javax.inject.Inject

class ErrorFragment : BaseFragment() {

    @Inject
    lateinit var retrySubject: PublishSubject<Unit>

    companion object {
        fun newInstance(): ErrorFragment {
            return ErrorFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.error_fragment_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.retry_button.clicks()
                .doOnNext {
                    activity?.supportFragmentManager?.inTransaction { remove(this@ErrorFragment) }
                } //TODO: non so se sia bello
                .subscribe(retrySubject::onNext)
                .addTo(compositeDisposable)
    }

}