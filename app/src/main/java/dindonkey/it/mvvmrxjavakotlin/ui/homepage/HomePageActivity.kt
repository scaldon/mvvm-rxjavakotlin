package dindonkey.it.mvvmrxjavakotlin.ui.homepage

import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import dagger.android.support.DaggerAppCompatActivity
import dindonkey.it.mvvmrxjavakotlin.R
import dindonkey.it.mvvmrxjavakotlin.ui.homepage.favourites.FavuoritesFragment
import dindonkey.it.mvvmrxjavakotlin.ui.homepage.flyers.FlyersFragment
import dindonkey.it.mvvmrxjavakotlin.ui.homepage.showcase.ShowcaseFragment

class HomePageActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar: Toolbar = configureToolbar()
        configureDrawer(toolbar)
        configureViewPager()
    }

    private fun configureDrawer(toolbar: Toolbar) {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.open_drawer,
                R.string.close_drawer)

        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
    }

    private fun configureToolbar(): Toolbar {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val actionbar: ActionBar? = supportActionBar
        actionbar?.apply {
            //            setDisplayHomeAsUpEnabled(true) //TODO: fare chiarezza
        }
        return toolbar
    }

    private fun configureViewPager() {
        val viewPager: ViewPager = findViewById(R.id.home_viewpager)

        viewPager.adapter = HomePageFragmentPagerAdapter(
                supportFragmentManager,
                arrayOf(ShowcaseFragment.newInstance(),
                        FlyersFragment.newInstance(),
                        FavuoritesFragment.newInstance()),
                arrayOf(getString(R.string.home_tab),
                        getString(R.string.flyers_tab),
                        getString(R.string.favourites_tab))
        )

        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)
    }


    class HomePageFragmentPagerAdapter(fm: FragmentManager?,
                                       private val fragments: Array<Fragment>,
                                       private val titles: Array<String>) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            return fragments[position]
        }

        override fun getCount(): Int {
            return fragments.size
        }

        override fun getPageTitle(position: Int): CharSequence {
            return titles[position]
        }

    }

}
