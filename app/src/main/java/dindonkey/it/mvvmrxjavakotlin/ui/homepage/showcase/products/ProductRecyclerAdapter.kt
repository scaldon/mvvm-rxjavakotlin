package dindonkey.it.mvvmrxjavakotlin.ui.homepage.showcase.products

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import dindonkey.it.mvvmrxjavakotlin.R.layout.discount_cardview
import dindonkey.it.mvvmrxjavakotlin.extensions.discountPercentage
import dindonkey.it.mvvmrxjavakotlin.extensions.load
import dindonkey.it.mvvmrxjavakotlin.extensions.priceFormat
import dindonkey.it.mvvmrxjavakotlin.extensions.strikeTroughText
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.discount_cardview.view.*

//TODO: passare la composite subscription
class ProductRecyclerAdapter(private val context: Context?,
                             private var dataSet: List<Product> = emptyList()) : RecyclerView.Adapter<ProductRecyclerAdapter.ProductHolder>() {

    val rxDataSet = PublishSubject.create<List<Product>>()

    init {
        rxDataSet.subscribe {
            dataSet = it
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductHolder {
        val cardView = LayoutInflater.from(context).inflate(discount_cardview, parent, false)
        return ProductHolder(cardView)
    }

    override fun onBindViewHolder(holder: ProductHolder, position: Int) {
        val product = dataSet[position]
        holder.productNameTextView.text = product.productName
        holder.productImageView load product.image
        holder.minPriceTextView.text = priceFormat(product.minPrice)
        product.oldPrice?.let {
            holder.oldPriceTextView strikeTroughText priceFormat(it)
            holder.discountPercentage.text = discountPercentage(it, product.minPrice)
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    class ProductHolder(v: View) : RecyclerView.ViewHolder(v) {
        val productNameTextView: TextView = v.productNameTextView
        val productImageView: ImageView = v.productImageView
        val oldPriceTextView: TextView = v.oldPrice
        val minPriceTextView: TextView = v.minPrice
        val discountPercentage: TextView = v.discountPercentage
    }
}


