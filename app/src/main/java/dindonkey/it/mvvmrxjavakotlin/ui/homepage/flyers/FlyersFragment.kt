package dindonkey.it.mvvmrxjavakotlin.ui.homepage.flyers

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dindonkey.it.mvvmrxjavakotlin.R

class FlyersFragment : Fragment() {

    companion object {
        fun newInstance(): FlyersFragment {
            return FlyersFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.flyers_fragment_layout, container, false)
    }
}