package dindonkey.it.mvvmrxjavakotlin.ui.homepage.showcase

import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dindonkey.it.mvvmrxjavakotlin.R
import dindonkey.it.mvvmrxjavakotlin.extensions.addTo
import dindonkey.it.mvvmrxjavakotlin.extensions.inTransaction
import dindonkey.it.mvvmrxjavakotlin.rx.SchedulerProvider
import dindonkey.it.mvvmrxjavakotlin.ui.BaseFragment
import dindonkey.it.mvvmrxjavakotlin.ui.ErrorFragment
import dindonkey.it.mvvmrxjavakotlin.ui.homepage.showcase.products.ProductRecyclerAdapter
import kotlinx.android.synthetic.main.showcase_fragment_layout.*
import javax.inject.Inject

class ShowcaseFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: ShowcaseViewModel

    @Inject
    lateinit var schedulerProvider: SchedulerProvider

    val errorFragment = ErrorFragment.newInstance()

    companion object {
        fun newInstance(): ShowcaseFragment {
            return ShowcaseFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.showcase_fragment_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        bindDiscounts()
        bindLoadingView()
        bindErrorView()
    }

    private fun bindDiscounts() {
        discounts.layoutManager = GridLayoutManager(context, 1, GridLayoutManager.HORIZONTAL, false)
        val discountsAdapter = ProductRecyclerAdapter(context)
        discounts.adapter = discountsAdapter

        viewModel.discounts
                .subscribeOn(schedulerProvider.computation())
                .observeOn(schedulerProvider.ui())
                .subscribe(discountsAdapter.rxDataSet::onNext)
                .addTo(compositeDisposable)
    }

    private fun bindLoadingView() {
        viewModel.loading
                .subscribeOn(schedulerProvider.computation())
                .observeOn(schedulerProvider.ui())
                .subscribe {
                    content.visibility = if (it) View.GONE else View.VISIBLE
                    progressBar.visibility = if (it) View.VISIBLE else View.GONE
                }
                .addTo(compositeDisposable)
    }

    private fun bindErrorView() {
        viewModel.error
                .subscribeOn(schedulerProvider.computation())
                .observeOn(schedulerProvider.ui())
                .subscribe {
                    activity?.supportFragmentManager?.inTransaction {
                        add(android.R.id.content, errorFragment)
                    }
                }
                .addTo(compositeDisposable)
    }

}