package dindonkey.it.mvvmrxjavakotlin.ui.homepage.favourites

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dindonkey.it.mvvmrxjavakotlin.R

class FavuoritesFragment : Fragment() {

    companion object {
        fun newInstance(): FavuoritesFragment {
            return FavuoritesFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.favourites_fragment_layout, container, false)
    }
}