package dindonkey.it.mvvmrxjavakotlin.ui

import android.content.Context
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable

open class BaseFragment : DaggerFragment() {
    lateinit var compositeDisposable: CompositeDisposable

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        compositeDisposable = CompositeDisposable()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.clear()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }
}