package dindonkey.it.mvvmrxjavakotlin.ui.homepage.showcase.products

import com.google.gson.annotations.SerializedName

data class Product(
        val productName: String,
        val oldPrice: String?,
        @SerializedName("minprice") val minPrice: String,
        val image: String)