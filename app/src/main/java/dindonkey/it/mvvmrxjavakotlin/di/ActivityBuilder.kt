package dindonkey.it.mvvmrxjavakotlin.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import dindonkey.it.mvvmrxjavakotlin.ui.homepage.HomePageActivity

@Module
abstract class ActivityBuilder {

    //qui potrei definire dipendenze specifiche per singole activity in moduli separati
    @ActivityScope
    @ContributesAndroidInjector
    abstract fun homePageActivity(): HomePageActivity

}