package dindonkey.it.mvvmrxjavakotlin.di

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import dindonkey.it.mvvmrxjavakotlin.network.MobileApiService
import dindonkey.it.mvvmrxjavakotlin.rx.AppSchedulerProvider
import dindonkey.it.mvvmrxjavakotlin.rx.SchedulerProvider
import io.reactivex.subjects.PublishSubject
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideSchedulerProvider(): SchedulerProvider {
        return AppSchedulerProvider()
    }

    @Provides
    @Singleton
    fun provideMobileService(schedulerProvider: SchedulerProvider): MobileApiService {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl("https://api.7pixel.it") //TODO: prendere da context
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(schedulerProvider.computation()))
                .build()

        return retrofit.create(MobileApiService::class.java)
    }

    @Provides
    @Singleton
    fun providesRetrySubject(): PublishSubject<Unit> {
        return PublishSubject.create()
    }

}
