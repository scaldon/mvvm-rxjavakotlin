package dindonkey.it.mvvmrxjavakotlin.di

import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dindonkey.it.mvvmrxjavakotlin.App
import javax.inject.Singleton

@Singleton
@Component(modules = [
    (AppModule::class),
    (ActivityBuilder::class),
    (FragmentBuilder::class),
    (AndroidSupportInjectionModule::class)
])
interface AppComponent: AndroidInjector<App> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()
}