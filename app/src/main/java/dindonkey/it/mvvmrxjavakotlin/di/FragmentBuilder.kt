package dindonkey.it.mvvmrxjavakotlin.di


import dagger.Module
import dagger.android.ContributesAndroidInjector
import dindonkey.it.mvvmrxjavakotlin.ui.ErrorFragment
import dindonkey.it.mvvmrxjavakotlin.ui.homepage.showcase.ShowcaseFragment

@Module
abstract class FragmentBuilder {

    //qui potrei definire dipendenze specifiche per singoli fragment
    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun showcaseFragment(): ShowcaseFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun errorFragment(): ErrorFragment

}
