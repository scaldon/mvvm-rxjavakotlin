package dindonkey.it.mvvmrxjavakotlin.extensions

import android.widget.ImageView
import com.squareup.picasso.Picasso


infix fun ImageView.load(url: String) {
    Picasso.get()
            .load(url)
            .into(this)
}