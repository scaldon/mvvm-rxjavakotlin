package dindonkey.it.mvvmrxjavakotlin.extensions

import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.Observer
import io.reactivex.functions.Consumer
import io.reactivex.functions.Function
import io.reactivex.subjects.BehaviorSubject
import java.util.concurrent.Callable

class RxActivityIndicator:  Observable<Boolean>() {

    private val counter = BehaviorSubject.createDefault(0)
    private val loading: Observable<Boolean>

    init {
        loading = counter
                .onErrorReturnItem(0)
                .map { it > 0 }
                .share()
    }

    override fun subscribeActual(observer: Observer<in Boolean>) {
        loading.subscribe(observer)
    }


    fun <T> trackActivityOfObservable(source: Observable<T>): Observable<T> {
        val resourceSupplierCallable = Callable<Unit> {
            increment()
        }

        val sourceSupplierFunction = Function<Unit, ObservableSource<T>> {
            x -> source
        }

        val disposerConsumer = Consumer<Unit> {
            unit -> decrement()
        }
        return Observable.using(resourceSupplierCallable, sourceSupplierFunction, disposerConsumer)
    }

    private fun increment() {
        counter.value?.let {
            counter.onNext(it.plus(1))
        }

    }

    private fun decrement() {
        counter.value?.let {
            counter.onNext(it.minus(1))
        }
    }

}

inline fun <reified T> Observable<T>.trackActivity(activityIndicator: RxActivityIndicator): Observable<T> {
    return activityIndicator.trackActivityOfObservable(this)
}