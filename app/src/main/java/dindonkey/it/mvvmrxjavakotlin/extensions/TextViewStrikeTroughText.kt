package dindonkey.it.mvvmrxjavakotlin.extensions

import android.graphics.Paint
import android.widget.TextView

infix fun TextView.strikeTroughText(text: String) {
    paintFlags = paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
    setText(text)
}