package dindonkey.it.mvvmrxjavakotlin.extensions

fun priceFormat(price: String): String {
    return String.format("€ %s", price)
}

fun discountPercentage(oldPrice: String, newPrice: String): String {
    val oldPriceDouble = oldPrice.replace(',', '.').toDouble()
    val newPriceDouble = newPrice.replace(',', '.').toDouble()
    val discountPercentage = (((oldPriceDouble - newPriceDouble) * 100) / oldPriceDouble).toInt()
    return String.format("-%d%%", discountPercentage)
}
