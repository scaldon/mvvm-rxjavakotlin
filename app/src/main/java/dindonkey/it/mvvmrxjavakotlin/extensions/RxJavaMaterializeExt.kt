package dindonkey.it.mvvmrxjavakotlin.extensions

import io.reactivex.Notification
import io.reactivex.Observable

inline fun <reified T> Observable<Notification<T>>.elements(): Observable<T> {
    return filter { it.isOnNext }.map { it.value }
}

inline  fun <reified T> Observable<Notification<T>>.error(): Observable<Throwable> {
    return filter { it.isOnError }.map { it.error }
}