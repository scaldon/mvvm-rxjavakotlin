package dindonkey.it.mvvmrxjavakotlin

import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import dindonkey.it.mvvmrxjavakotlin.di.DaggerAppComponent


class App : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().create(this)
    }

}
