package dindonkey.it.mvvmrxjavakotlin

import dindonkey.it.mvvmrxjavakotlin.extensions.addTo
import dindonkey.it.mvvmrxjavakotlin.ui.homepage.showcase.ShowcaseViewModel
import dindonkey.it.mvvmrxjavakotlin.stubs.MobileApiServiceErrorStub
import dindonkey.it.mvvmrxjavakotlin.stubs.MobileApiServiceStub
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Test

class ShowcaseViewModelTests {

    private val reloadSubject = PublishSubject.create<Unit>()

    @Test
    fun discounts() {
        val showcaseViewModel = ShowcaseViewModel(MobileApiServiceStub(), reloadSubject)

        showcaseViewModel.discounts
                .test()
                .assertValue { "product1" == it[0].productName }
                .assertNoErrors()

        showcaseViewModel.error
                .test()
                .assertNoValues()
    }

    @Test
    fun errors() {
        val error = Exception()
        val showcaseViewModel = ShowcaseViewModel(MobileApiServiceErrorStub(error), reloadSubject)

        showcaseViewModel.error
                .test()
                .assertNoErrors()
                .assertValueCount(1)
                .assertValue(error)

        showcaseViewModel.discounts
                .test()
                .assertNoValues()
    }

    @Test
    fun one_call_with_multiple_subscription() {
        val mobileApiService = MobileApiServiceStub()
        val showcaseViewModel = ShowcaseViewModel(mobileApiService, reloadSubject)
        val compositeDisposable = CompositeDisposable()

        showcaseViewModel.discounts
                .subscribe()
                .addTo(compositeDisposable)

        showcaseViewModel.error
                .subscribe()
                .addTo(compositeDisposable)

        assertFalse(compositeDisposable.isDisposed)
        assertEquals(1, mobileApiService.callCount)

    }

    @Test
    fun reload_triggers_another_call() {
        val mobileApiService = MobileApiServiceStub()
        val showcaseViewModel = ShowcaseViewModel(mobileApiService, reloadSubject)
        val compositeDisposable = CompositeDisposable()

        showcaseViewModel.discounts
                .subscribe()
                .addTo(compositeDisposable)

        reloadSubject.onNext(Unit)

        assertEquals(2, mobileApiService.callCount)
    }
}

