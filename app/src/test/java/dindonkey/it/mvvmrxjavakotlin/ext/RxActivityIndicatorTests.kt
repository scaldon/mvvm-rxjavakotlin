package dindonkey.it.mvvmrxjavakotlin.ext

import dindonkey.it.mvvmrxjavakotlin.extensions.RxActivityIndicator
import dindonkey.it.mvvmrxjavakotlin.extensions.trackActivity
import io.reactivex.Observable
import io.reactivex.schedulers.TestScheduler
import org.junit.Test
import java.util.concurrent.TimeUnit

class RxActivityIndicatorTests {

    @Test
    fun trackActivity() {
        val activityIndicator = RxActivityIndicator()
        val testScheduler = TestScheduler()
        val observableToTrack = Observable.just("").delay(3, TimeUnit.SECONDS, testScheduler)
        val trackActivityObserver = activityIndicator.test()

        val observer = observableToTrack.trackActivity(activityIndicator).test()

        trackActivityObserver.assertValues(false, true)

        testScheduler.advanceTimeBy(5, TimeUnit.SECONDS)

        observer.assertComplete()
        trackActivityObserver.assertValues(false, true, false)

    }

}