package dindonkey.it.mvvmrxjavakotlin.stubs

import dindonkey.it.mvvmrxjavakotlin.network.DiscountsResponse
import dindonkey.it.mvvmrxjavakotlin.network.MobileApiService
import io.reactivex.Observable

class MobileApiServiceErrorStub(private val error: Throwable) : MobileApiService {

    override fun discounts(): Observable<DiscountsResponse> {
        return Observable.create {
            it.onError(error)
        }
    }

}