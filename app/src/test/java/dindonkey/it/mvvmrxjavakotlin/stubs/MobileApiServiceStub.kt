package dindonkey.it.mvvmrxjavakotlin.stubs

import dindonkey.it.mvvmrxjavakotlin.ui.homepage.showcase.products.Product
import dindonkey.it.mvvmrxjavakotlin.network.DiscountedProducts
import dindonkey.it.mvvmrxjavakotlin.network.DiscountsResponse
import dindonkey.it.mvvmrxjavakotlin.network.MobileApiService
import io.reactivex.Observable

class MobileApiServiceStub : MobileApiService {

    var callCount = 0

    override fun discounts(): Observable<DiscountsResponse> {

        val discountsResponse = DiscountsResponse(
                DiscountedProducts(listOf(
                        Product("product1", "99.99", "10.99", "image"),
                        Product("product2", "99.99", "10.99", "image")
                ))
        )

        return Observable.create {
            callCount++
            it.onNext(discountsResponse)
            it.onComplete()
        }

    }
}