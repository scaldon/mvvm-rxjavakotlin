package dindonkey.it.mvvmrxjavakotlin.fragments

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import dindonkey.it.mvvmrxjavakotlin.FragmentTestActivity
import dindonkey.it.mvvmrxjavakotlin.R
import dindonkey.it.mvvmrxjavakotlin.ui.ErrorFragment
import io.reactivex.subjects.PublishSubject
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ErrorFragmentTests {

    private val fragment = ErrorFragment.newInstance()
    private val testRetrySubject: PublishSubject<Unit> = PublishSubject.create()

    @get:Rule
    val rule = object: ActivityTestRule<FragmentTestActivity>(FragmentTestActivity::class.java) {
        override fun afterActivityLaunched() = runOnUiThread {
            //viene passato il metodo inject di questo test come "injector". Verrà quindi chiamato da dagger
            //quando dovrà usare il supportFragmentInjector
            activity.startFragment(fragment, this@ErrorFragmentTests::inject)
        }
    }

    fun inject(fragment: ErrorFragment) {
        fragment.retrySubject = testRetrySubject
    }

    @Test
    fun click_on_retry_subject() {
        val retryObserver = testRetrySubject.test()

        onView(withId(R.id.retry_button)).perform(click())

        retryObserver
                .assertNoErrors()
                .assertValueCount(1)
    }
}
