package dindonkey.it.mvvmrxjavakotlin.fragments

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import dindonkey.it.mvvmrxjavakotlin.FragmentTestActivity
import dindonkey.it.mvvmrxjavakotlin.FragmentTests
import dindonkey.it.mvvmrxjavakotlin.R
import dindonkey.it.mvvmrxjavakotlin.matchers.ViewMatchers.Companion.atPosition
import dindonkey.it.mvvmrxjavakotlin.ui.homepage.showcase.ShowcaseFragment
import dindonkey.it.mvvmrxjavakotlin.ui.homepage.showcase.ShowcaseViewModel
import io.reactivex.subjects.PublishSubject
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class ShowcaseFragmentTests: FragmentTests() {

    private val fragment = ShowcaseFragment.newInstance()
    private val testRetrySubject: PublishSubject<Unit> = PublishSubject.create()

    @get:Rule
    val rule = object: ActivityTestRule<FragmentTestActivity>(FragmentTestActivity::class.java) {
        override fun afterActivityLaunched() = runOnUiThread {
            activity.startFragment(fragment, this@ShowcaseFragmentTests::inject)
        }
    }

    fun inject(fragment: ShowcaseFragment) {
        fragment.viewModel = ShowcaseViewModel(mobileApiServiceMock(), testRetrySubject)
        fragment.errorFragment.retrySubject = testRetrySubject
        fragment.schedulerProvider = testSchedulerProvider
    }



    @Test
    fun test_discounts() {
        onView(withId(R.id.discounts))
                .check(matches(atPosition(0, hasDescendant(
                                withText("DISUGUAL NATURAL SNACK STICK MONOPROTEICO POLLO 10 GR")))))

    }

    @Test
    fun show_error_fragment() {
        networkBehavior.setFailurePercent(100)
        onView(withId(R.id.retry_button))
                .check(matches(isDisplayed()))
    }


}