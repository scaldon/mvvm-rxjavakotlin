package dindonkey.it.mvvmrxjavakotlin

import dindonkey.it.mvvmrxjavakotlin.rx.SchedulerProvider
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers.trampoline

class TestSchedulerProvider:SchedulerProvider {
    override fun ui(): Scheduler {
        return trampoline()
    }

    override fun computation(): Scheduler {
        return trampoline()
    }

}