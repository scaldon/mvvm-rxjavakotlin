package dindonkey.it.mvvmrxjavakotlin

import dindonkey.it.mvvmrxjavakotlin.network.MobileApiService
import dindonkey.it.mvvmrxjavakotlin.stubs.MobileApiServiceMock
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.mock.MockRetrofit
import retrofit2.mock.NetworkBehavior

open class FragmentTests {
    val testSchedulerProvider = TestSchedulerProvider()
    val networkBehavior = NetworkBehavior.create()

    fun mobileApiServiceMock(): MobileApiServiceMock {
        val retrofit = Retrofit.Builder()
                .baseUrl("https://mah/meh/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(testSchedulerProvider.computation()))
                .build()
        val mockRetrofit = MockRetrofit.Builder(retrofit)
                .networkBehavior(networkBehavior)
                .build()
        val delegate = mockRetrofit.create(MobileApiService::class.java)

        val mobileApiServiceMock = MobileApiServiceMock(delegate)
        return mobileApiServiceMock
    }
}