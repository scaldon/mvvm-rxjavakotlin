package dindonkey.it.mvvmrxjavakotlin.stubs

import dindonkey.it.mvvmrxjavakotlin.network.DiscountedProducts
import dindonkey.it.mvvmrxjavakotlin.network.DiscountsResponse
import dindonkey.it.mvvmrxjavakotlin.network.MobileApiService
import dindonkey.it.mvvmrxjavakotlin.ui.homepage.showcase.products.Product
import io.reactivex.Observable
import retrofit2.mock.BehaviorDelegate

class MobileApiServiceMock(val behaviorDelegate: BehaviorDelegate<MobileApiService>) : MobileApiService {

    //TODO: portare fuori
    var testDiscounts = listOf(
            Product(
            "DISUGUAL NATURAL SNACK STICK MONOPROTEICO POLLO 10 GR",
            "999,99",
            "99,99",
            "https://host/image.jpg")
    )

    override fun discounts(): Observable<DiscountsResponse> {
        val discountsResponse = DiscountsResponse(DiscountedProducts(testDiscounts))
        return behaviorDelegate.returningResponse(discountsResponse).discounts()
    }

}